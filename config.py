import os
from celery.schedules import crontab

basedir = os.path.abspath(os.path.dirname(__file__))


class Config():
    '''Config settings class.'''
    ERROR_LOG_FILE = 'app/errors.log'
    POSTS_PER_PAGE = 20

    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access']

    ELASTICSEARCH_URL = 'http://localhost:9200'
    SECRET_KEY = '*******************************'
        
    SEARCH_INDEX = 'data_covid'




    SEARCH_INDEX = 'forum_data'
    HOSTNAME = 'http://127.0.0.1:5000'

    COVID_DATA_URL = "https://raw.githubusercontent.com/CSSEGISandData/" +\
        "COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/" +\
        "{}.csv"
    COVID_DATA_INDEX = 'covid_stats'
    COVID_DATA_MAPPING = {
        "settings": {
            "number_of_shards": 2,
            "number_of_replicas": 1
        },
        "mappings": {
            "properties": {
                "FIPS": {
                    "type": "integer"
                },
                "Admin2": {
                    "type": "text"
                },
                "Province_State": {
                    "type": "text"
                },
                "Country_Region": {
                    "type": "text"
                },
                "Last_Update": {
                    "type": "date",
                    "format": "yyyy-MM-dd HH:mm:ss"
                },
                "Location": {
                    "type": "geo_point",
                    "ignore_malformed": True
                },
                "Confirmed": {
                    "type": "long"
                },
                "Deaths": {
                    "type": "long"
                },
                "Recovered": {
                    "type": "long"
                },
                "Active": {
                    "type": "long"
                },
                "Combined_Key": {
                    "type": "text"
                }
            }
        }
    }


CELERY_CONFIG = {
    'broker_url': 'redis://localhost:6379/0',
    'result_backend': 'redis://localhost:6379/0',
    'task_track_started': True,
    'beat_schedule': {
        "five-minute-task": {
            "task": "tasks.refresh_index",
            "schedule": crontab(minute='*/5'),
            "args": ['forum_data']
        }
    }
}
