from app import db, app
from app.model import User, Question, Answer


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Question': Question, 'Answer': Answer}
