from flask import render_template, redirect, flash, url_for, request, session
from flask_login import current_user, login_user, login_required, logout_user
from app.model import User
from werkzeug.urls import url_parse
from app.auth import bp
from app.auth.forms import LoginForm, RegistrationForm
import requests


host = 'http://127.0.0.1:5000'


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))

    form = LoginForm()

    username = form.username.data
    password = form.password.data

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        login_response = requests.post('{0}/api/login'.format(host), json={
            "username": username, "password": password})

        session['api_token'] = login_response.json().get('acess_token')

        if user is None or not user.check_password(password):
            flash('Invalid username or password')

            return redirect(url_for('auth.login'))

        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')

        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('main.index')

        return redirect(next_page)

    return render_template('auth/login.html', title='Sign In', form=form)


@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('main.index'))


@bp.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))

    form = RegistrationForm()
    username = form.username.data
    email = form.email.data
    password = form.password.data
    sign_up_response = requests.post('{0}/api/signup'.format(host), json={
        "username": username, "email": email, "password": password})

    if sign_up_response.status_code == 400:
        sign_up_response_dict = sign_up_response.json()
        flash(sign_up_response_dict.get('msg'))

    if sign_up_response.status_code == 200:
        sign_up_response_dict = sign_up_response.json()
        flash(sign_up_response_dict.get('msg'))
        user = User.query.filter_by(username=username).first()

        login_user(user, remember=False)
        return redirect(url_for('auth.user', user_id=user.id))

    return render_template('auth/register.html', title='Register', form=form)


@bp.route('/user/<int:user_id>')
@login_required
def user(user_id):
    response_user = requests.get(
        '{0}/api/user/{1}'.format(host, user_id), headers={
            'Authorization': 'Bearer {}'.format(session['api_token'])})

    user_data = response_user.json()
    response_questions = requests.get(
     '{0}/api/user/{1}/questions'.format(host, user_id),
     headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

    questions_by_user_id = {}
    questions = []

    if response_questions.status_code == 400:
        response = response_questions.json()
        flash(response.get('msg'))

    if response_questions.status_code == 200:
        questions_by_user_id = response_questions.json()

        for ques in reversed(questions_by_user_id['question']):
            questions.append(ques.get('body'))

    if questions_by_user_id != {}:
        return render_template(
            'user1.html', user_data=user_data, questions=questions,
            questions_by_user_id=questions_by_user_id)

    return render_template(
        'user1.html', user_data=user_data, questions=questions)
