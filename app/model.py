from hashlib import md5
from app import db, login
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from flask import Flask
from app.search import add_to_index, remove_from_index, query_index

app = Flask(__name__)


class SearchableMixin(object):
    @classmethod
    def search(cls, expression, page, per_page):
        ids, total = query_index(cls.__tablename__, expression, page, per_page)
        if total == 0:
            return cls.query.filter_by(id=0), 0
        when = []
        for i in range(len(ids)):
            when.append((ids[i], i))
        return cls.query.filter(cls.id.in_(ids)).order_by(
            db.case(when, value=cls.id)), total

    @classmethod
    def before_commit(cls, session):
        session._changes = {
            'add': list(session.new),
            'update': list(session.dirty),
            'delete': list(session.deleted)
        }

    @classmethod
    def after_commit(cls, session):
        for obj in session._changes['add']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['update']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['delete']:
            if isinstance(obj, SearchableMixin):
                remove_from_index(obj.__tablename__, obj)
        session._changes = None

    @classmethod
    def reindex(cls):
        for obj in cls.query:
            add_to_index(cls.__tablename__, obj)


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    questions = db.relationship('Question', backref='author', lazy='dynamic',
                                cascade="all, delete-orphan")
    answers = db.relationship('Answer', backref='author', lazy='dynamic',
                              cascade="all, delete-orphan")
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        url = 'http://www.gravatar.com/avatar/{}?s={}&d=identicon'
        return url.format(digest, size)

    def to_dict(self):
        data = {
            'id': self.id,
            'username': self.username,
            'last_seen': self.last_seen.isoformat() + 'Z',
            'about_me': self.about_me,
            'avatar': self.avatar(128)
        }
        return data


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class Question(SearchableMixin, db.Model):
    __tablename__ = "question"
    __searchable__ = ['body']
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    answers = db.relationship('Answer', backref='Solution', lazy='dynamic',
                              cascade="all, delete-orphan")
    tags = db.relationship('TagQuestion', backref='Tags', lazy='dynamic', cascade='all, delete-orphan')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<question {} >'.format(self.body)

    def to_dict(self):
        data = {
            'id': self.id,
            'body': self.body,
            'user_id': self.user_id,
            'time_stamp': self.timestamp.isoformat() + 'Z',
            'author': self.author.to_dict()

           }

        return data


class TagQuestion(db.Model):
    __tablename__ = "TableQuestion"
    id = db.Column(db.Integer, primary_key=True)
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'))
    name = db.Column(db.String(150))
    slug_field = db.Column(db.String(150))

    def __repr__(self):
        return '<tag {} >'.format(self.name)

    def to_dict(self):

        data = {
            'id': self.id,
            'question_id': self.question_id,
            'name': self.name,
            'slug_field': self.slug_field,

           }
        return data


class Answer(SearchableMixin, db.Model):
    __tablename__ = "answer"
    __searchable__ = ['body']
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.Text)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'))

    def __repr__(self):
        return '<answer {} >'.format(self.body)

    def to_dict(self):

        data = {
            'id': self.id,
            'body': self.body,
            'user_id': self.user_id,
            'question_id': self.question_id,
            'time_stamp': self.timestamp.isoformat() + 'Z',
            'author': self.author.to_dict()

           }
        return data


db.event.listen(db.session, 'before_commit', SearchableMixin.before_commit)
db.event.listen(db.session, 'after_commit', SearchableMixin.after_commit)
