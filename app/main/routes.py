from app import db
from flask import render_template, redirect, flash, url_for, request
from flask import current_app, g
from app.main.forms import QuestionForm, EditProfileForm, AnswerForm
from app.main.forms import SearchForm
from flask_login import current_user, login_required
from app.model import User, Question, Answer
from datetime import datetime
from app.main import bp
import requests
from app.auth.routes import session, host


@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
        g.search_form = SearchForm()


@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    form = QuestionForm()
    questions_json_list = []

    if form.validate_on_submit():
        body = form.question.data
        resp_ques_post = requests.post(
          '{0}/api/user/question'.format(host),
          json={"body": body},
          headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

        if resp_ques_post.status_code == 200 or 401:
            flash('you succesfully posted')
            return redirect(url_for('main.index'))

    resp_ques_get = requests.get(
        '{0}/api/questions'.format(host),
        headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

    if resp_ques_get.status_code == 200:
        questions_json = resp_ques_get.json()
        questions_json_list = questions_json['question']

    if questions_json_list != []:

        return render_template("index.html", title='Home Page', form=form,
                               questions_json_list=questions_json_list)

    return render_template("index.html",
                           title='Home Page', form=form, User=User)


@bp.route('/user/<int:user_id>')
@login_required
def user(user_id):
    response_user = requests.get(
        '{0}/api/user/{1}'.format(host, user_id),
        headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

    user_data = response_user.json()
    response_questions = requests.get(
        '{0}/api/user/{1}/questions'.format(host, user_id),
        headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

    questions_by_user_id = {}
    questions = []

    if response_questions.status_code == 400:
        response = response_questions.json()
        flash(response.get('msg'))

    if response_questions.status_code == 404:
        flash('page not found')

    if response_questions.status_code == 200:
        questions_by_user_id = response_questions.json()

        for ques in reversed(questions_by_user_id['question']):
            questions.append(ques.get('body'))

    if questions_by_user_id != {}:
        return render_template('user1.html', user_data=user_data,
                               questions=questions,
                               questions_by_user_id=questions_by_user_id)

    return render_template('user1.html', user_data=user_data,
                           questions=questions)


@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)

    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data

        response_user_put = requests.put(
         '{0}/api/user'.format(host),
         json={"username": form.username.data, "about_me": form.about_me.data},
         headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

        if response_user_put.status_code == 200 or 400:
            flash(response_user_put.json()['msg'])
            return redirect(url_for('main.index'))

        return redirect(url_for('main.edit_profile'))

    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me

    return render_template('edit_profile.html', title='Edit Profile',
                           form=form)


@bp.route('/question/<int:ques_id>', methods=['GET', 'POST'])
@login_required
def answer(ques_id):
    form = AnswerForm(current_user)
    response_ques = requests.get(
        '{0}/api/question/{1}'.format(host, ques_id),
        headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

    question = {}
    answers_json_list = []

    if response_ques.status_code == 200:
        question = response_ques.json()

    if form.validate_on_submit():
        body = form.answer.data
        resp_ans_post = requests.post(
            '{0}/api/user/question/{1}/answer'.format(host, ques_id),
            json={"body": body}, headers={
                'Authorization': 'Bearer {}'.format(session['api_token'])})

        if resp_ans_post.status_code == 200 or 400:
            flash('you succesfully posted')
            return redirect(url_for('main.answer', ques_id=ques_id))

    resp_ans_get = requests.get(
        '{0}/api/answers/{1}'.format(host, ques_id),
        headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

    if resp_ans_get.status_code == 200:
        answers_json = resp_ans_get.json()
        answers_json_list = answers_json['answers']

    if answers_json_list != [] and question:

        return render_template("get_answer.html", title='Home Page',
                               question=question,
                               form=form,
                               answers_json_list=answers_json_list,
                               ques_id=ques_id)

    if answers_json_list != []:
        return render_template("get_answer.html",
                               title='Home Page', form=form,
                               answers_json_list=answers_json_list)
    else:
        return render_template("get_answer.html",
                               title='Home Page', form=form)


@bp.route('/delete_user', methods=['GET'])
@login_required
def delete_user():
    response_user_del = requests.delete(
        '{0}/api/user/'.format(host),
        headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

    if response_user_del.status_code == 200 or 400 or 401:
        flash(response_user_del.json()['msg'])
        return redirect(url_for('main.index'))


@bp.route('/delete/answer/<answer_id>')
@login_required
def delete_answer(answer_id):
    response_ans_del = requests.delete(
        '{0}/api/user/answer/{1}'.format(host, answer_id),
        headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

    if response_ans_del.status_code == 200 or 400 or 401 or 404:
        flash(response_ans_del.json()['msg'])
        return redirect(url_for('main.index'))


@bp.route('/edit/question/<ques_id>/answer/<answer_id>',
          methods=['GET', 'POST'])
@login_required
def edit_answer(answer_id, ques_id):
    form = AnswerForm(current_user)
    response_ans = requests.get(
        '{0}/api/answer/{1}'.format(host, answer_id),
        headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

    if response_ans.status_code == 200:
        answer_by_id = response_ans.json()
        answer_by_id['answers']['body'] = form.answer.data

    if form.validate_on_submit():
        response_ans_put = requests.put(
         '{0}/api/user/answer/{1}'.format(host, answer_id),
         json={"body": form.answer.data},
         headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

        if response_ans_put.status_code == 200 or 400 or 404:
            flash(response_ans_put.json()['msg'])
            return redirect(url_for('main.answer', answer_id=answer_id,
                            ques_id=ques_id))

    elif request.method == 'GET':
        form.answer.data = answer_by_id['answers']['body']
        return render_template('edit_answer.html', title='Edit Answer',
                               form=form)


@bp.route('/edit/question/<ques_id>', methods=['GET', 'POST'])
@login_required
def edit_question(ques_id):

    form = QuestionForm()
    response_ques = requests.get(
        '{0}/api/question/{1}'.format(host, ques_id),
        headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

    if response_ques.status_code == 200:
        ques_by_id = response_ques.json()
        ques_by_id['question']['body'] = form.question.data

    if form.validate_on_submit():
        response_ques_put = requests.put(
         '{0}/api/user/question/{1}'.format(host, ques_id),
         json={"body": form.question.data},
         headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

        if response_ques_put.status_code == 200 or 400 or 404:
            flash(response_ques_put.json()["msg"])
            return redirect(url_for('main.index'))

    elif request.method == 'GET':
        form.question.data = ques_by_id['question']['body']
        return render_template('edit_question.html', title='Edit Question',
                               form=form)


@bp.route('/delete/question/<int:ques_id>')
@login_required
def delete_question(ques_id):
    response_ques_del = requests.delete(
     '{0}/api/user/question/{1}'.format(host, ques_id),
     headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

    if response_ques_del.status_code == 200 or 400 or 401 or 404:
        flash(response_ques_del.json()['msg'])
        return redirect(url_for('main.index'))


@bp.route('/search')
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('main.index'))
    page = request.args.get('page', 1, type=int)
    questions, total = Question.search(g.search_form.q.data, page,
                                       current_app.config['POSTS_PER_PAGE'])
    answers, total = Answer.search(g.search_form.q.data, page,
                                   current_app.config['POSTS_PER_PAGE'])
    next_url = url_for('main.search_question', q=g.search_form.q.data,
                       page=page + 1) \
        if total > page * current_app.config['POSTS_PER_PAGE'] else None
    prev_url = url_for('main.search_question', q=g.search_form.q.data,
                       page=page - 1) \
        if page > 1 else None

    return render_template('search.html', title='Search',
                           questions=questions,
                           answers=answers,
                           next_url=next_url, prev_url=prev_url, User=User)


# @bp.route('/tag/questions/<int:tag_id>')
# @login_required
# def questions_by_tag(tag_id):
#     questions = Question.query.filter_by(tags=tag_id)
#     questions_by_tag = requests.get(
#         '{0}/api/questions'.format(host),
#         headers={'Authorization': 'Bearer {}'.format(session['api_token'])})

#     return render_template('index.html', {'questions': questions})
