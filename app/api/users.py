from app.model import User, Question, Answer, TagQuestion
from flask_jwt_extended import create_access_token, jwt_required
from flask_jwt_extended import get_jwt_identity, get_raw_jwt
import datetime
from app import jwt, db, app, blacklist
from flask import request, jsonify
from app.api import bp
from app.background_jobs import update_covid_stats


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return jti in blacklist


@bp.route('/api/signup', methods=['POST'])
def signup():
    '''Signup new user
       return type <dict>'''
    users = User.query.all()
    json_data = request.get_json()
    new_user = User(username=json_data['username'], email=json_data['email'],
                    about_me="", last_seen=datetime.datetime.utcnow())
    new_user.set_password(json_data['password'])

    for user in users:
        if user.username == new_user.username:
            return jsonify({'msg': 'username already exists'}), 400

        if user.email == new_user.email:
            return jsonify({'msg': 'email already exists'}), 400

    db.session.add(new_user)
    db.session.commit()

    return jsonify({'msg': 'you have successfully signed up'})


@bp.route('/api/login', methods=['POST'])
def login():
    '''return login request
       return type <dict>'''
    json_data = request.get_json()
    username = json_data['username']
    password = json_data['password']
    user = User.query.filter_by(username=username).first()

    if user is None:
        return jsonify({'msg': 'no user found'})

    elif not user.check_password(password):
        return jsonify({'msg': 'wrong username or password'})

    expires_delta = datetime.timedelta(days=10)
    token = create_access_token(identity=str(user.id),
                                expires_delta=expires_delta)

    return jsonify({'acess_token': token})


@bp.route('/api/users', methods=['GET'])
@jwt_required
def get_all_users():
    '''return all the users
       return type <dict>'''
    users = User.query.all()

    output = []

    if not users:
        return jsonify({'msg': 'users not found'}), 404

    for user in users:
        output.append(user.to_dict())

    return jsonify({'users': output})


@bp.route('/api/user/<int:user_id>', methods=['GET'])
@jwt_required
def get_one_user(user_id):
    '''return one the user data
       return type <dict>'''
    user = User.query.filter_by(id=user_id).first()

    if not user:
        return jsonify({'msg': 'No user found'}), 404

    user_data = user.to_dict()

    return jsonify({'user': user_data})


@bp.route('/api/user', methods=['PUT'])
@jwt_required
def update_user():
    '''Upadate  the user data
       return type <dict>'''
    users = User.query.all()
    user = User.query.filter_by(id=get_jwt_identity()).first()

    for user_exists in users:
        if user != user_exists:
            if request.json['username'] == user_exists.username:
                return jsonify({'msg': 'Username already exist'})

    user.username = request.json['username']
    user.about_me = request.json['about_me']
    db.session.commit()

    return jsonify({'msg': 'the user has been promoted'})


@bp.route('/api/user/', methods=['DELETE'])
@jwt_required
def delete_user():
    '''delete the user data
       return type <dict>'''
    user = User.query.filter_by(id=get_jwt_identity()).first()
    db.session.delete(user)
    db.session.commit()

    return jsonify({'msg': 'you have been sucessfully deleted'})


@bp.route('/api/user/question', methods=['POST'])
@jwt_required
def create_question():
    '''Create the question
        return type <dict>'''
    user = User.query.filter_by(id=get_jwt_identity()).first()
    data = request.json['body']
    new_ques = Question(body=data, user_id=user.id)
    db.session.add(new_ques)
    db.session.commit()

    return jsonify({'msg': 'you sucessfully created new question'})


@bp.route('/api/user/<int:user_id>/questions', methods=['GET'])
@jwt_required
def get_all_question_of_user(user_id):
    '''return all the question by user
       return type <dict>'''
    user = User.query.filter_by(id=user_id).first()
    if not user:
        return jsonify({'msg': 'No user found'}), 404

    ques = user.questions.all()
    if not ques:
        return jsonify({'msg': 'No ques found'}), 404

    output = []

    for que in ques:
        output.append(que.to_dict())

    return jsonify({'question': output})


@bp.route('/api/questions', methods=['GET'])
@jwt_required
def get_all_question():
    '''return all the question
        return type <dict>'''
    ques = Question.query.all()
    if not ques:
        return jsonify({'msg': 'No ques found'}), 404

    output = []

    for que in ques:
        output.append(que.to_dict())

    return jsonify({'question': output})


@bp.route('/api/question/<int:ques_id>', methods=['GET'])
@jwt_required
def get_one_question(ques_id):
    '''return all the question
        return type <dict>'''
    ques = Question.query.filter_by(id=ques_id).first()

    if not ques:
        return jsonify({'msg': 'No ques found'}), 404

    output = ques.to_dict()

    return jsonify({'question': output})


@bp.route('/api/user/question/<int:ques_id>', methods=['PUT'])
@jwt_required
def update_question(ques_id):
    '''delete the question text
        return type <dict>'''
    user = User.query.filter_by(id=get_jwt_identity()).first()
    ques = Question.query.filter_by(id=ques_id, user_id=user.id).first()
    if not ques:
        return jsonify({'msg': 'No ques found'}), 404

    ques.body = request.json['body']
    db.session.commit()

    return jsonify({'msg': 'you sucessfully updated'})


@bp.route('/api/user/question/<int:ques_id>', methods=['DELETE'])
@jwt_required
def delete_question(ques_id):
    '''Delete the question text
        return type <dict>'''
    user = User.query.filter_by(id=get_jwt_identity()).first()
    ques = Question.query.filter_by(
        id=ques_id, user_id=user.id).first()

    if not ques:
        return jsonify({'msg': 'No ques found'}), 404

    db.session.delete(ques)
    db.session.commit()

    return jsonify({'msg': 'you sucessfully deleted'})


@bp.route('/api/user/question/<ques_id>/answer', methods=['POST'])
@jwt_required
def create_answer(ques_id):
    '''Create the answer of given question id
       return type <dict>'''
    user = User.query.filter_by(id=get_jwt_identity()).first()
    data = request.json['body']
    new_ans = Answer(body=data, user_id=user.id, question_id=ques_id)

    db.session.add(new_ans)
    db.session.commit()

    return jsonify({'msg': 'you sucessfully answered'})


@bp.route('/api/user/<int:user_id>/answers', methods=['GET'])
@jwt_required
def get_all_answers_of_user(user_id):
    '''return all the answers by user
       return type <dict>'''
    user = User.query.filter_by(id=user_id).first()
    answers = user.answers.all()

    if not user:
        return jsonify({'msg': 'No user found'}), 404

    if not answers:
        return jsonify({'msg': 'No answers found'}), 404

    output = []

    for ans in answers:
        output.append(ans.to_dict())

    return jsonify({'answers': output})


@bp.route('/api/answers', methods=['GET'])
@jwt_required
def get_all_answers():
    '''return all the answers
        return type <dict>'''
    answers = Answer.query.all()

    if not answers:
        return jsonify({'msg': 'No answer found'}), 404

    output = []

    for ans in answers:
        output.append(ans.to_dict())

    return jsonify({'answers': output})


@bp.route('/api/answer/<int:ans_id>', methods=['GET'])
@jwt_required
def get_one_answer(ans_id):
    '''return all the answers
        return type <dict>'''
    answer = Answer.query.filter_by(id=ans_id).first()

    if not answer:
        return jsonify({'msg': 'No ques found'}), 404

    output = answer.to_dict()

    return jsonify({'answers': output})


@bp.route('/api/answers/<int:ques_id>', methods=['GET'])
@jwt_required
def get_all_answers_by_ques_id(ques_id):
    '''return all the answers
        return type <dict>'''
    answers = Answer.query.filter_by(question_id=ques_id).all()

    if not answers:
        return jsonify({'msg': 'No answers found'}), 404

    output = []
    for ans in answers:
        output.append(ans.to_dict())

    return jsonify({'answers': output})


@bp.route('/api/user/answer/<int:ans_id>', methods=['PUT'])
@jwt_required
def update_answer(ans_id):
    '''delete the answer text
        return type <dict>'''
    user = User.query.filter_by(id=get_jwt_identity()).first()
    ans = Answer.query.filter_by(id=ans_id, user_id=user.id).first()

    if not ans:
        return jsonify({'msg': 'No ans found'}), 404

    ans.body = request.json['body']
    db.session.commit()

    return jsonify({'msg': 'you sucessfully updated'})


@bp.route('/api/user/answer/<int:ans_id>', methods=['DELETE'])
@jwt_required
def delete_answer(ans_id):
    '''Delete the answer text
        return type <dict>'''
    user = User.query.filter_by(id=get_jwt_identity()).first()

    if not user:
        return jsonify({'msg': 'user Not found'}), 404

    if str(user.id) != get_jwt_identity():
        return jsonify({'msg': 'you are not allowed'}), 400

    ans = Answer.query.filter_by(
        id=ans_id, user_id=user.id).first()

    if not ans:
        return jsonify({'msg': 'No answer found or you are not allowed'}), 404

    db.session.delete(ans)
    db.session.commit()

    return jsonify({'msg': 'you sucessfully deleted'})


@bp.route('/api/tag/<int:ques_id>', methods=['POST'])
def create_tag(ques_id):
    json_data = request.get_json()
    ques_tag = TagQuestion(name=json_data['name'], slug_field=json_data['slug_field'], question_id=ques_id)
    db.session.add(ques_tag)
    db.session.commit()

    return jsonify({'msg': 'you sucessfully posted tag'})


@bp.route('/api/tag/<int:ques_id>', methods=['GET'])
def get_tag(ques_id):
    tags_ques = TagQuestion.query.all()
    if not tags_ques:
        return jsonify({'msg': 'tag not found'}), 404

    output = []

    for tag in tags_ques:
        output.append(tag.to_dict())

    return jsonify({'tag_question': output})


@bp.route('/api/tag/question/<int:tag_id>', methods=['GET'])
def get_all_questions_by_tag_id(tag_id):
    questions_by_tag_id = Question.query.filter(Question.tags.any(id=tag_id)).all()
    if not questions_by_tag_id:
        return jsonify({'msg': 'tag not found'}), 404

    output = []

    for ques in questions_by_tag_id:
        output.append(ques.to_dict())

    return jsonify(
        {'tag_details': {'tag_id': tag_id,
                         'question_detail': output
                        }
        }
        )


@bp.route('/api/logout', methods=['DELETE'])
@jwt_required
def logout():
    jti = get_raw_jwt()['jti']
    blacklist.add(jti)
    return jsonify({"msg": "Successfully logged out"}), 200


@bp.route('/api/covid_stats', methods=['PUT'])
def update_covid_data_api():
    '''Handles PUT requests to update the Covid infection stats from a csv file
    on the internet(the task is run asynchronously), and GET requests to check
    the task status.'''
    task = update_covid_stats.delay(app.config['COVID_DATA_INDEX'])
    return {"msg": "Updating covid stats in the background",
            "task_id": task.task_id}
