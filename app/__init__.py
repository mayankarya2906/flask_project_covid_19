from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from elasticsearch import Elasticsearch
from flask_bootstrap import Bootstrap
from flask_jwt_extended import JWTManager
from celery import Celery
from config import CELERY_CONFIG


app = Flask(__name__)
app.config.from_object(Config)
app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']])\
    if app.config['ELASTICSEARCH_URL'] else None

celery = Celery(app.name)
celery.conf.update(CELERY_CONFIG)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
jwt = JWTManager(app)
blacklist = set()
LOG_FILE = app.config['ERROR_LOG_FILE']
HOST = app.config['HOSTNAME']
login = LoginManager(app)
login.login_view = 'auth.login'
bootstrap = Bootstrap(app)


from app.errors import bp as errors_bp
app.register_blueprint(errors_bp)

from app.auth import bp as auth_bp
app.register_blueprint(auth_bp)

from app.main import bp as main_bp
app.register_blueprint(main_bp)

from app.api import bp as api_bp
app.register_blueprint(api_bp)

from app import model, errors
