**Project Description**

    In this project we make our own API using flask and sqlite as database.
    Here we use the concept of authorization and authentication using jwt library.
    In request user has to pass x-access-token which is only generated for admin only.
    Basically,we use our API only for CRUD operation.

    After making API we just make User interface or make a website in which user can make his account and can ask question about the Corona virus and other person can give answer.

    I use API request call to render the HTML template on the screen using jijna2.

    I also use Celery as task queue to do the background jobs in such a way that while loading the data the server is prevent from the  crashed.

    Here I also use the elastic search for makin serach engine in my website.

**Pre-requisite**

    I Assume that you already have python3 in your system.

**Requirements** 

    The Requirements that is there in the requirement.txt file.
    Please Stasify the requirement using the following command.

    pip3 install -r requirement.txt

**Instruction**

    for making my code run in your device please follow the below intructions:-
    1.Please make virtual environment using the command python3 -m venv <name of vitual env>
    2.then please use the command source <name of your virtual env>/bin/activate to activate the enviroment
    3.after this you can use pip3 install -r requirement.txt
    4. then only use command flask run in your root directory

    Please refer to Postman Collection and API_documentation.pdf, if you have any doubt in API

    To run this project you need a secret key.

    for getting secret key you contact with me.
    mayankarya2906@gmail.com

    for running redis server use the commands given in installing_and_starting_redis.sh

    After to run the celery server use following command:-
    celery -A app.celery worker --loglevel=info

    You have to need three server at a time redis-server(broker) , celery-server(worker),main server





**Directory Structure**

    flask_project
    ├── README.md
    ├── app.db
    ├── config.py
    ├── novel_covid.py
    ├── installing_and_starting_redis.sh
    ├── .gitignore
    |
    ├── app
    │   ├── __init__.py
    │   ├── model.py
    │   ├── search.py
    │   ├── background_jobs.py
    │   ├── api
    │   │   ├── __init__.py
    │   │   ├── users.py
    |   |   |
    │   └── templates
    |       |   |   └── auth
    |       |   |    ├── login.html
    │       |   |    └── register.html
    |       |   └── errors
    |       |       ├── 404.html
    |       |       └── 500.html        
    │       ├── _answers.html
    │       ├── _questions.html
    │       ├── base.html
    │       ├── edit_answer.html
    │       └── edit_question.html
    │       └── get_answer.html
    │       └── index.html
    │       └── search.html
    │   ├── auth
    |       ├── __init__.py
    │       ├── forms.py
    │       └── routes.py
    |   ├── errors       
    │       ├── __init__.py
    │       └── handlers.py
    |   ├── main
    |         ├── __init__.py
    |         ├── forms.py
    |         └── routes.py
    |
    ├── Postman_collection
    |     |
    |     └── postman_collection.json
    |
    └── wire_frames
         ├── signup.png
         ├── login.png
         ├── edit_profile.png
         ├── edit_answer.png
         ├── edit_question.png
         ├── answer.png
         └── questions.png



